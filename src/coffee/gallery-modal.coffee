$ ->

	setViewAttr = () ->
		if $(window).width() > 992
			$('.gancy').each ->
				$(@).attr('data-view', 'true')
		else if $(window).width() < 992
			$('.gancy').each ->
				$(@).attr('data-view', 'false')

	setViewAttr()




	$(document).on  'click','[data-view="true"]', ->
		el = $(@)
		modal = $('.gallery-popup')
		ind = el.data('item')
		getWindowHeight = $(window).height()
		slide = modal.find '[data-slide="'+ind+'"]'

		modal
			.addClass 'show'
			.find '.slides'
			.find '[data-slide="'+ind+'"]'
			.addClass 'visible'
			.siblings('.item')
			.removeClass 'visible'
			.addClass 'hidden'

		modal.find '[data-slide] img'
			.css 
				'max-height': getWindowHeight - 200 + 'px'

		modal
			.find '[data-thumb="'+ind+'"]'
			.addClass 'active'
			.siblings('.item')
			.removeClass 'active'

	$(window).on 'resize', ->
		setViewAttr()
		$('[data-slide] img')
			.css 
				'max-height': $(this).height() - 200 + 'px'

	$('[data-thumb]').on 'click', ->
		$(@)
			.addClass 'active'
			.siblings()
			.removeClass 'active'

		$('[data-slide="'+$(@).data('thumb')+'"]')
			.addClass 'visible'
			.siblings()
			.removeClass 'visible'

		$('[data-slide="'+$(@).data('thumb')+'"] img')
 			.css
 				'max-height': $(window).height() - 200 + 'px'

	$('.close').on 'click', ->
		$(@).parents('.gallery-popup').removeClass 'show'

	$('.overlay').on 'click', ->
		$('.gallery-popup').removeClass 'show'
	
	$('.gallery-popup .arr').on 'click', ->
			if $(@).hasClass('arr-prev')
				if !$('[data-thumb].active').is(':first-child')
					$('[data-thumb].active').prev().trigger('click')
				else
					$('[data-thumb]').last().trigger('click')
			else if $(@).hasClass('arr-next')
				if !$('[data-thumb].active').is(':last-child')
					$('[data-thumb].active').next().trigger('click')
				else
					$('[data-thumb]').first().trigger('click')

	return
