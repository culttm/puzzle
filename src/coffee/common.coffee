$ ->
    do ->
        owl =  $('.posters-carousel')

        owl.on 'initialized.owl.carousel change.owl.carousel refreshe.owl.carousel', (event) ->
            if event.namespace?
                carousel = event.relatedTarget
                element = event.target
                current = carousel.current()
                
                $('.owl-next', element).toggleClass('disabled', current == carousel.maximum())

                $('.owl-prev', element).toggleClass('disabled', current == 0)

                $('[data-share-index="'+current+'"]')
                    .addClass 'visible'
                    .siblings('[data-share-index]')
                    .removeClass 'visible'

            return

        owl.owlCarousel({
            items: 3
            loop: false
            autoWidth:true  
            nav:true
            navText: []
            info: (data) ->
                console.log data
            responsive: 
                320 : {
                    items: 1
                    autoWidth: false
                }
                480 : {
                    items: 1
                    autoWidth: false
                }
                768 : {
                    items: 1
                    autoWidth: false
                }
                1000 : {
                    loop: false
                    items: 3
                    autoWidth: false
                }
        })




        $('html')
            .removeClass 'no-js'
            .addClass 'complete'
        totalSlides = $('#page > .section').length

        
        $('#page').fullpage({
                fitToSection: false
                autoScrolling: false
                scrollBar: true
                continuousVertical: true
                onLeave: (index, nextIndex, direction) ->
                    console.log index
                    setActiveClass nextIndex
                    setNavFixed nextIndex
                    setVisibleMobileAnchors nextIndex
                    chekVisNavArrows nextIndex
                    setNavPosition nextIndex
            })

        $.fn.fullpage.setMouseWheelScrolling(false);

        setActiveClass = (ind) ->
            anchor = $('.section')
                        .eq(ind - 1)
                        .data 'anchor'
            $('.anchors *').removeClass 'active'
            $('[href="#'+anchor+'"]')
                        .parent()
                        .addClass 'active'

        setNavFixed = (ind) ->
            if ind > 1
                $('.main-top-nav').addClass 'hide'
                setTimeout ( =>
                    $('.mhc').removeClass('hide').addClass('vis')
                ), 500
            else
                $('.main-top-nav').removeClass 'hide'
                setTimeout ( =>
                    $('.mhc').removeClass('vis').addClass('hide')
                ), 500

        setVisibleMobileAnchors = (ind) ->
            section = $('.section-2')
            contWidth = section.find('.mobile-anchors').width()
            element = section.find('[data-anchor]')
            elementWidth = element.width()
            element = section.find(element).length
            getElCount = Math.floor(contWidth/elementWidth)

            i = ind - getElCount
            if ind > getElCount
                $('[data-anchor="'+(i - 1)+'"]')
                    .removeClass 'vis'
                    .addClass 'hidden'
            else if ind <= getElCount
                $('[data-anchor="'+(ind - 1)+'"]')
                    .addClass 'visible'
                    .removeClass 'hidden'

        setVisibleMobileAnchors()

        # checkScrollBar = (w) ->
            # if w < 992
            #     $.fn.fullpage.setAutoScrolling(false)
            # else
            #     $.fn.fullpage.setAutoScrolling(true)

        # checkScrollBar($(window).width())

        chekVisNavArrows = (ind) ->
            $('.arrow-top').toggleClass 'hide', ind == 1
            $('.arrow-bottom').toggleClass 'hide', ind == totalSlides

            return


        # $('.nav-arrow').on 'click', ->
        #     if $(@).hasClass 'arrow-top'
        #         $.fn.fullpage.moveSectionUp();
        #     else if $(@).hasClass 'arrow-bottom'
        #         $.fn.fullpage.moveSectionDown();

        $('.nav-arrow').on 'click', ->
            active = $('.section.active')
            if $(@).hasClass 'arrow-top'
                a = active.prev('.section').offset().top
                console.log a
                $('body').animate
                    scrollTop: a
                    400
                # $.fn.fullpage.moveSectionUp();
            else if $(@).hasClass 'arrow-bottom'
                # $.fn.fullpage.moveSectionDown();
                a = active.next('.section').offset().top
                console.log a
                $('body').animate
                    scrollTop: a
                    400
        $('.js-slide-to').on 'click', ->
            target = $(@).data 'target'
            a = $('.section-'+target).offset().top
            console.log a
            $('body').animate
                scrollTop: a
                400


        # $('.js-slide-to').on 'click', ->
        #     target = $(@).data 'target'
        #     $.fn.fullpage.moveTo(target)
            
        

        $(window).on 'resize', ->
            setVisibleMobileAnchors()
            setNavPositionLeft() 
            # checkScrollBar($(window).width())


        $(document).on 'click', '.print', ->
            URL = $(@).data('picture')
            if URL?
                w = window.open(URL)
                w.window.print()

        setNavPosition = (i) ->
            nav = $('.section-navigation')
            h = nav.height()

            nav.css 'margin-top', -(h/2)+53

            nav.toggleClass 'hidden', i == 1  
        
        setNavPositionLeft = ->
            et = $('.bd')
            left = Math.floor(et.position().left)
            top = Math.floor(et.position().top)

            
            $('.section-navigation').css 'left', left
            $('.nav-arrows').css
                "left": left + et.width()
            return


        setNavPositionLeft()


        # $('.arrow-top').on 'click', ->
        #     $('a[href="#video"]').on('mousedown')

        return