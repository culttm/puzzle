publishFB = (options) ->
	feed = {
		method: options.method,
		name: options.name,
		link: options.link,
		picture: options.picture,
		caption: options.caption,
		description: options.description
	}

	callback = (response) ->
		if response? and response.post_id?
			console.log response
	FB.ui(feed, callback)

$ ->
	$('[data-provider]').on 'click', (e) ->
		e.preventDefault()
		
		data = $(@).data('provider-data')
		

		if $(@).data('provider') == 'facebook'
			if data?
				method = data.method || 'feed'
				link = data.link
				picture = data.picture
				name = data.name
				caption = data.caption
				description = data.description || ' '

				publishFB
					method: method
					link: link
					picture: picture
					name: name
					caption: caption,
					description: description

		if $(@).data('provider') == 'twitter'
			if data?
				url = "https://twitter.com/share?url="+
					encodeURIComponent(data.link) + "&text="+
					encodeURIComponent(data.name + ' #' + data.caption)
				window.open(url, '', 'toolbar=0,status=0,width=554,height=436, top=100, left=200')
		

		if $(@).data('provider') == 'odnoklassniki'
			if data?
				l = data.link.split("#")[0]
				surl = l + 'data/share.php?image='+
					encodeURIComponent(data.picture) + "&title=" +
					encodeURIComponent(data.name) + "&description=" +
					encodeURIComponent(data.description.substr(0, 240)) + "&redirect=" +
					encodeURIComponent(data.link)

				url = "http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=" + encodeURIComponent(surl)
				window.open(url, '', 'toolbar=0,status=0,width=554,height=436, top=100, left=200')
		return
			